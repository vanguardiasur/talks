\beamer@endinputifotherversion {3.36pt}
\select@language {spanish}
\beamer@sectionintoc {1}{Rompiendo el hielo}{2}{0}{1}
\beamer@sectionintoc {2}{¿De qué vamos a hablar?}{8}{0}{2}
\beamer@sectionintoc {3}{Empezamos con algunas definiciones}{12}{0}{3}
\beamer@sectionintoc {4}{¿Qué hace cada componente?}{19}{0}{4}
\beamer@sectionintoc {5}{¿De dónde obtenemos cada componente?}{27}{0}{5}
\beamer@sectionintoc {6}{Las elecciones para nuestra querida {\relax \fontsize {24.88}{30}\selectfont CIAA NXP}}{32}{0}{6}
\beamer@sectionintoc {7}{Repasando}{43}{0}{7}
\beamer@sectionintoc {8}{Requisitos mínimos}{49}{0}{8}
\beamer@sectionintoc {9}{Limitaciones de Linux en la CIAA NXP}{62}{0}{9}
\beamer@sectionintoc {10}{Conclusión}{70}{0}{10}
